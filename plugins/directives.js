/**
 * @author -> Dare Adewole, Favour.adewole@gmail.com
 * This file contains all my custom directives and its resource variables
 */

import Vue from 'vue'

const patternList = {
	name: /^[a-z]+[a-z\s.,-]*[a-z.]+$/ig,
	email: /^[a-z0-9_.]+@[a-z]+\.[a-z]{2,3}$/ig,
	phone: /^\+*[0-9]{7,11}$/g
}

Vue.directive('type', {
	inserted(el, binding, vnode) {
		el.addEventListener('keyup', () => {
			const isValid = el.value.match(patternList[binding.arg]) !== null
			vnode.context[`${binding.expression}`] = isValid
		}, false)
	}
})

Vue.directive('left', {
	inserted(el) {
		el.style.textAlign = 'left'
	}
})


Vue.directive('align', {
	inserted(el, binding) {
		el.style.textAlign = binding.args
	}
})

Vue.directive('right', {
	inserted(el) {
		el.style.textAlign = 'right'
	}
})

Vue.directive('center', {
	inserted(el) {
		el.style.textAlign = 'center'
	}
})
