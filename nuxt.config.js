module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Echwood Limited',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Echwood is pioneering technology and business solutions that impact the everyday life of businesses. We partner with SMEs to develop innovative systems, products and applications that delivers business success with ease and simplicity. We are your technology and business solutions partners. We offer advisory services that drives the market in all' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/img/echwood.ico' },
      { rel: 'stylesheet', type: 'text/css', href: '/css/animate.css' },
      { rel: 'stylesheet', type: 'text/css', href: '/css/foundation.css' },
      { rel: 'stylesheet', type: 'text/css', href: '/css/uikit.min.css' }
    ],
    script: [
      { src: '/js/particles.js' },
      { src: '/js/uikit.min.js' },
      { src: '/js/uikit-icons.min.js' }

    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },

  /**
   * Calling plugins
   */
  plugins: [
    '~/plugins/directives.js',
    // '~/plugins/oldmodel.js'
    // { src: '~/plugins/uikit.js', ssr: false }
  ],

  modules: [
    // provide path to the file with resources
    // [
    //   'nuxt-sass-resources-loader',
    //   [
    //     // '@assets/scss/_variables.scss',
    //     // '@assets/scss/app.scss',
    //     // '@assets/scss/index.scss',
    //     // '@assets/scss/mixins/index.scss'
    //   ]
    // ]
  ],

  /**
   * Calling Css
   */

  css: [
    // 'uikit/dist/css/uikit.css'
  ],

  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        // config.module.rules.push({
        //   enforce: 'pre',
        //   test: /\.(js|vue)$/,
        //   // loader: 'eslint-loader',
        //   exclude: /(node_modules)/
        // })
      }
    }
  }
}

